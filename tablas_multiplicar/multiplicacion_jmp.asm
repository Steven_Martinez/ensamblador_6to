;************Ensamblador*****************
;************6to "A"*********************
;************Angel Steven Martinez Chamba
;************miercoles 05 de agosto del 2020
section .data

    msj db " x "
	len equ $-msj
	
	msj2 db " = "
	len2 equ $-msj2
		
	salto db 10
	len_esp equ $-salto

section .bss
    a   resb 2
    b   resb 2
    c   resb 2

section .text
    global _start	
_start:

    mov al, 3
    add al, '0'
	mov [a], al
	mov cx, 1

ciclo:
	push cx
	mov ax, [a]

	sub ax, '0'
	mul cx

	add ax, '0'
	mov [c], ax
	
	add cx, '0'
	mov [b], cx

	call imprimir_multiplicando
	call imprimir_x
	call imprimir_multiplicador
	call imprimir_igual
	call imprimir_producto
	call imprimir_salto
		
	pop cx
	inc cx
	cmp cx, 10
	jnz ciclo

imprimir_multiplicando: 
	mov eax, 4
	mov ebx, 1
	mov ecx, a
	mov edx, 1
	int 80h
	ret

imprimir_multiplicador: 
	mov eax, 4
	mov ebx, 1
	mov ecx, b
	mov edx, 1
	int 80h
	ret

imprimir_producto: 
	mov eax, 4
	mov ebx, 1
	mov ecx, c
	mov edx, 1
	int 80h
	ret

imprimir_x: 
	mov eax, 4
	mov ebx, 1
	mov ecx, msj
	mov edx, len
	int 80h
	ret

imprimir_igual: 
	mov eax, 4
	mov ebx, 1
	mov ecx, msj2
	mov edx, len2
	int 80h
	ret

imprimir_salto: 
	mov eax, 4
	mov ebx, 1
	mov ecx, salto
	mov edx, len_esp
	int 80h
	ret

salir:
    mov eax, 1
    int 80h
;************Ensamblador*****************
;************6to "A"*********************
;************Angel Steven Martinez Chamba
;************miercoles 05 de agosto del 2020
section .data
	
	msj2 db " x "
	len2 equ $-msj2
	
	msj3 db " = "
	len3 equ $-msj3
	
	msj4 db " | "
	len4 equ $-msj4
	
	espacio db 10
	len_esp equ $-espacio
	
section .bss
	n1 rest 2
	n2 rest 2
	resp rest 2
	aux rest 2

section .text
	global _start
	
_start:
	mov al, 1
	mov [aux], al
	mov rcx, 0
	
principal:
	cmp rcx, 9
	jz ciclo
	inc rcx
	push rcx
	mov [resp], rcx
	jmp imp
	
imp:
	mov al, [resp]
	mov [n1], al
	mov cl, [aux]
	mul cl
	
	mov [resp], al
	mov ah, [n1]
	add ax, '00'
	add cx, '00'
	mov [n1], ah
	mov [n2], cl

	call imprimir_n1
	call imprimir_msg2
	call imprimir_n2
	call imprimir_msg3
	
	mov eax, 48
	add [resp], eax 

	call imprimir_resp
	call imprimir_msg4

	pop rcx
	jmp principal

ciclo: 
	call imprimir_espacio
	mov ebx, [aux]
	inc ebx
	mov [aux], ebx
	mov ecx, 0
	cmp ebx, 10
	jb principal

imprimir_n1: 
	mov eax, 4
	mov ebx, 1
	mov ecx, n1
	mov edx, 2
	int 80h
	ret

imprimir_n2: 
	mov eax, 4
	mov ebx, 1
	mov ecx, n2
	mov edx, 2
	int 80h
	ret

imprimir_resp: 
	mov eax, 4
	mov ebx, 1
	mov ecx, resp
	mov edx, 2
	int 80h
	ret

imprimir_msg2: 
	mov eax, 4
	mov ebx, 1
	mov ecx, msj2
	mov edx, len2
	int 80h
	ret

imprimir_msg3: 
	mov eax, 4
	mov ebx, 1
	mov ecx, msj3
	mov edx, len3
	int 80h
	ret

imprimir_msg4: 
	mov eax, 4
	mov ebx, 1
	mov ecx, msj4
	mov edx, len4
	int 80h
	ret

imprimir_espacio: 
	mov eax, 4
	mov ebx, 1
	mov ecx, espacio
	mov edx, len_esp
	int 80h
	ret
	
salir:
	mov eax, 1
	int 80h
;*****************Ensamblador*****************
;*****************Autor***********************
;*********Angel Steven Martinez Chamba********
;*****************6to "A"*********************
;************miercoles 22 de julio del 2020*******
;Programa que imprime asteriscos en cuadrado
%macro impirmir 1
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, 1
	int 80h
%endmacro

section .data
    msg db '*',10
    salto db 10,' '

section .text
    global _start
_start:

    mov bx, 9					;filas
    mov cx, 9					;columnas
             
principal:
	push bx						;añadir a la pila
	cmp bx, 0					;compara bx = 0
	jz salir					;salir
	jmp asterisco 				

asterisco:
	dec cx						;decrementa cx
	push cx
	impirmir msg				;el valor de cx se remplaza con asterisco
	pop cx
	cmp cx, 0
	jg asterisco				;jg, verifica que el primer operando sea mayor al segundo
	impirmir salto
	pop bx
	dec bx
	mov cx, 9					;retorna a 9
	jmp principal

salir:
    mov eax, 1
    int 80h

;Programa que imprime asteriscos
section .data
    asterisco db '*',10

section .text
    global _start
_start:

    mov rax, 9
    
imprimir:                   ;etiqueta
    dec rax                 ;decremento
    push rax                ;añadir a la pila
    mov rax, 4
    mov ebx, 1
    mov ecx, asterisco
    mov edx, 1
    int 80h
    pop rax                 
    cmp rax, 0
    jnz imprimir
    ;jmp imprimir

    ;finalizar el programa
    mov eax, 1
    int 80h
%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    msg db 10, "Hello Word: "
    len_msg equ $-msg

section .bss
    num resb 1

section .txt
    global _start
_start:

    mov rcx, 9

ciclo:
    push rcx
    add rcx, '0'
    mov [num], rcx

    imprimir msg, len_msg

    imprimir num, 1

    pop rcx

    loop ciclo
   
salir:
    mov eax, 1
    int 80h

section .data

    array db 3, 2, 0, 7, 5
    len_array equ $-array

section .bss

    numero resb 1

section .text
    global _start
_start:

                                    ;esi                    -> fijar tamaño real del array en memoria
                                    ;edi                    -> contener el indice del array
    mov esi, array
    mov edi, 0

imprimir:

    mov al,[esi]
	add al, '0'
	mov [numero], al

	add esi, 1
	add edi, 1	; [edi]

	mov eax,4
	mov ebx, 1
	mov ecx, numero
	mov edx, 1
	int 80h
	
	cmp edi, len_arreglo            ;cmp 3, 4 => activa carry	
			                        ;cmp 4, 3 => desactiva carry y zero
			                        ;cmp 3, 3 => desactiva carry y zero se activa		
	jb imprimir	                    ;se ejecuta cuando la banderra de carry esta activada

salir:
    mov eax, 1
    int 80h


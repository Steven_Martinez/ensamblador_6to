section .data
    
    msj db "Ingrese 5 numeros", 10
    len1 equ $-msj
    msj2 db "Numero menor es: ", 10
    len2 equ $-msj2 
    arreglo db 0,0,0,0,0
    len_arr equ $-arreglo

section .bss

    num resb 2

section .data
    global _start
_start:

    mov esi, arreglo
    mov edi, 0
    ;***********************mensaje 1*****************
    mov eax, 4
    mov ebx, 1
    mov ecx, msj
    mov edx, len1
    int 80h
    ;**********************lectura ********************
leer:
    mov eax, 3
    mov ebx, 0
    mov ecx, num
    mov edx, 2
    int 80h

    mov al, [num]
    sub al, '0'

    mov [esi], al       ;movemos el valor a un indice del arreglo

    inc edi
    inc esi             ;indice del arreglo

    cmp edi, len_arr
    jb leer

    mov ecx, 0
    mov bl, 0

comparacion:
    mov al, [arreglo + ecx]
    cmp al, bl
    jb contador
    mov bl, al

contador:
    inc ecx
    cmp ecx, len_arr
    jb comparacion

imprimir: 
    add bl, '0'
    mov [num], bl

    mov eax, 4
    mov ebx, 1
    mov ecx, msj2
    mov edx, len2
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, num
    mov edx, 1
    int 80h

exit:
    mov eax, 1
    int 80h
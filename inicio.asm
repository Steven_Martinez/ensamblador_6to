section .data
       message   "Hello world!!"   ; const message  1bit memoria
       longitud EQU $-message   ; const longitud calcula numero caracteres

;section .bss

section .text
    global _start
_start:
    ; imprimir message
    mov eax, 4 ; sistema interpreta como escribir   
    mov ebx, 1 ; puntero al valor que se pasa 
    mov ecx, message    ;movimiento al registro ecx
    mov edx, longitud
    int 80H             ;interruccion de software linux

    mov eax, 1  ; terminacion del programa
    mov ebx, 0
    int 80H
;Un macro procedimiento es un bloque con nombre de instrucciones en lenguaje ensamblador.
;Una vez definido, puede invocarse (llamarse) todas las veces que se desee en un programa.

; imprimir es el nombre del macro, 
; número de parámetros que recibirá el macro (2)
%macro imprimir 2 	
	mov eax,4  			; el sistema interpreta 4 como "escribir" mueve el valor al registro eax
	mov ebx,1			; salida estándar (imprimir en terminal) 
	mov ecx,%1 			; %1 indica que va a recibir el primer parámetro
	mov edx,%2			; %2 indica que va a recibir el segundo parámetro
	int 80H				; interruccion de kernel linux OS
%endmacro

; leer es el nombre del macro, 
; número de parámetros que recibirá el macro (2)
%macro leer 2			
    mov eax,3			; lectura
    mov ebx,0			; lectura por teclado
    mov ecx,%1 			; %1 indica que va a recibir el primer parámetro
    mov edx,%2			; %2 indica que va a recibir el segudo parámetro
    int 80H 			; interruccion de kernel linux OS
%endmacro				


; ecx,modo de acceso	----> comentario
; edx, permisos			----> comentario

section .bss				; sección en la que se definen datos sin inicializar.
	auxiliar resb 30		; deja un espacio de 30 bytes sin inicializar
	auxiliarb resb 30		; deja un espacio de 30 bytes sin inicializar
	auxiliarc resb 30		; deja un espacio de 30 bytes sin inicializar


section .data				; inicio seccion de datos
	msg db 0x1b ,"       " 	; 6 espacios para contener al dato
	lenmsg equ $-msg		; longitud de msg



	salto db " ",10 		; salto de linea
	lensalto equ $-salto	; longitud salto de linea




section .text				; inicio seccion de codigo
    global _start    		; indica donde comienza la ejecución del programa

_start:
	
	mov ecx,9				; conteo de bytes del proceso

	mov al,0				; sustraer el destino de cero
	mov [auxiliar],al

cicloI:						; etiqueta que inicia el bloque de codigo
	push ecx				; guarda el valor de ecx en la pila
	mov ecx,9				; mueve un valor al registro ecx
	mov al,0				; sustraer el destino de cero
	mov [auxiliarb],al

	cicloJ:
		push ecx			; guarda el valor de ecx en la pila


		call imprimir0al9	; salta a la dirección dada 
		

	;	imprimir msg2,lenmsg2

	fincicloJ:
		mov al,[auxiliarb]	; mover al registro al el valor de auxiliarb
		inc al				; incrementar en 1
		mov [auxiliarb],al	; mover el registro al incrementado en 1 

		pop ecx				; remueve de la cola
		loop cicloJ			; volver a la posicion con la etiqueta cicloJ
		
	;imprimir salto,lensalto

fincicloI:
	mov al,[auxiliar]		; mover al registro al el valor auxiliar
	inc al					; incremento en 1 al valor del registro
	mov [auxiliar],al		; mover el valor al incrementado en 1 al auxiliar

	pop ecx					; elimina(sacar) 	de la cola 
	loop cicloI				; volver a la posicion con la etiqueta cicloI
	

salir:						; etiqueta
	mov eax, 1				; instrucion de terminacion del programa
	int 80H					; interruccion de kernel linux OS



imprimir0al9:				; etiqueta 
	
	mov ebx,"["				; asignacion al registro ebx el caracter '['
	mov [msg+1], ebx		; asigna el valor del registro ebx a la constante en la posicion 1

	mov bl,[auxiliar]		; mover la referencia de memoria para apuntar al dato
	add bl,'0'				; pasar a cadena
	mov [msg+2], bl			; mover referencia de memoria en la posicion 2 para apuntar al dato


	mov ebx,";"				; mueve al registro ebx el caracter ';'
	mov [msg+3], ebx		; mueve el valor de ebx a la constante msg en la posición 3

	
	mov bl,[auxiliarb]		; mover al registro bl la referencia contenida en la varibale auxliarb
	add bl,'0'				; cambiar a cadena
	mov [msg+4],bl			; mover referencia de memoria en la posicion 4 para apuntar al dato

	mov ebx,"fJ"			; mover al registro ebx 
	mov [msg+5], ebx		; mover referencia de memoria en la posicion 5 para apuntar al dato

	imprimir msg,lenmsg		; llamado al macro imprimir pasandole 2 paramatro(msg,lenmsg)

	ret						; retorna-regresar a la linea donde se realizo la llamada

%macro imprimir 2
    mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 80h
%endmacro

%macro lectura 1
    mov eax, 3
	mov ebx, 0
	mov ecx, %1
	mov edx, 2
	int 80h
%endmacro

section .data
	msg_num1 db 10,'Ingrese el numero 1: ', 0
	len_msg_num1 equ $-msg_num1
 
	msg_num2 db	'Ingrese el numero 2: ', 0
	len_msg_num2 equ $-msg_num2
  
	msg_res	db 10,'Resultado xxx: ', 0
	len_msg_res	equ	$-msg_res
   
section .bss
	num1:		resb	2
	num2:		resb 	2
	resultado:	resb 	2
 
section .text 
	global _start
 
_start: 
	; *************lectura numero 1***************
	imprimir msg_num1, len_msg_num1
 
	lectura num1
 
	; *************lectura numero 2***************
	imprimir msg_num2, len_msg_num2
 
	lectura num2

 	;************* Inician los saltos*************** 
	jmp dividir 
 
sumar:
	mov al, [num1]
	mov bl, [num2]
 
	sub al, '0' 	; Convertimos los valores ingresados de ascii a decimal
	sub bl, '0'
 
	add al, bl	
 	add al, '0'
 
	mov [resultado], al

	mov [msg_res + 11], dword 'sum' 

	mov eax, 4
	mov ebx, 1
	mov ecx, msg_res
	mov edx, len_msg_res
	int 80h
 
	mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, 2
	int 80h
 
	jmp restar
 
restar:
	mov al, [num1]
	mov bl, [num2]
 
	sub al, '0'
	sub bl, '0'
 
	sub al, bl
 	add al, '0'
 
	mov [resultado], al
 
	mov [msg_res + 11], dword 'res' 

	mov eax, 4
	mov ebx, 1
	mov ecx, msg_res
	mov edx, len_msg_res
	int 80h
 
	mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, 1
	int 80h
 
	jmp multiplicar
 
multiplicar:
	mov al, [num1]
	mov bl, [num2]
 
	sub al, '0'
	sub bl, '0'
 
	mul bl 		; Multiplicamos. AX = AL X BL
	add ax, '0'
 
	mov [resultado], ax

	mov [msg_res + 11], dword 'mul'  

	mov eax, 4
	mov ebx, 1
	mov ecx, msg_res
	mov edx, len_msg_res
	int 80h
 
	mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, 1
	int 80h
 
	jmp exit
 
dividir:
	mov al, [num1]
	mov bl, [num2]
 
	mov dx, 0
	mov ah, 0
 
	sub al, '0'
	sub bl, '0'
 
	div bl
 
	add ax, '0'
 
	mov [resultado], ax

	mov [msg_res + 11], dword 'div'  

	mov eax, 4
	mov ebx, 1
	mov ecx, msg_res
	mov edx, len_msg_res
	int 80h
 
	mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, 1
	int 80h

	jmp sumar
 
exit: 
	mov eax, 1
	mov ebx, 0
	int 80h
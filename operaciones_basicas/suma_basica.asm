section .data  
    msg_num1 db "Ingrese el primer numero", 10
    len_msg1 equ $-msg_num1
    msg_num2 db "Ingrese el segundo numero", 10
    len_msg2 equ $-msg_num2
    msg_num3 db "Resultado", 10
    len_msg3 equ $-msg_num3

section .bss
    num1 resb 5
    num2 resb 5
    resp resb 5

section .text
    global _start
_start:
    ;******************imprime el mensaje***************
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_num1
    mov edx, len_msg1
    int 80h
    ;******************lectura de numero***************
    mov eax, 3                  ; define el tipo de operacion (lectura)
    mov ebx, 2                  ; estandar de entrada
    mov ecx, num1               ; captura por teclado
    mov edx, 5                  ; num de caracteres reservado
    int 80h
    ;******************imprime el mensaje***************
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_num2
    mov edx, len_msg2
    int 80h
    ;******************lectura de numero***************
    mov eax, 3                  ; define el tipo de operacion (lectura)
    mov ebx, 2                  ; estandar de entrada
    mov ecx, num2               ; captura por teclado
    mov edx, 5                  ; num de caracteres reservado
    int 80h
    ;******************imprime el mensaje***************
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_num3
    mov edx, len_msg3
    int 80h
    ;******************asignacion de la variable resp***************
    mov eax, [num1]
    sub eax, '0'
    mov ebx, [num2]
    sub ebx, '0'
    add eax, ebx
    add eax, '0'
    mov [resp], eax
    ;******************imprime el mensaje***************
    mov eax, 4
    mov ebx, 1
    mov ecx, resp
    mov edx, 5
    int 80h
    ;******************salir del sistema***************
    mov eax, 1                  ; terminacion del sistema
    int 80h                     ; interrucion de OS Linux
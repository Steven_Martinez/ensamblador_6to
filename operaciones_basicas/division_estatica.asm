section .data
	msg db "El resultado es:",10
	len equ $- msg
   msg2 db " ",10   

segment .bss
	res resb 1
	residuo resb 1
	cociente resb 1

section	.text
   global _start    
	
_start:            
   mov ax,'9'
   sub ax, '0'
	
   mov bl, '2'
   sub bl, '0'

   div bl
   add ax, '0'
	
   mov [res], ax
   mov ecx, msg	
   mov ecx, msg	
   mov edx, len
   mov ebx, 1	
   mov eax, 4	
   int 80h	
	
   mov ecx, res
   mov edx, 1
   mov ebx, 1	
   mov eax, 4	
   int 80h

   add al, '0'
   mov [residuo], al
   mov ecx, residuo
   mov edx, 1
   mov ebx, 1	
   mov eax, 4	
   int 80h

   add ah, '0'
   mov [cociente], ah
   mov ecx, cociente
   mov edx, 1
   mov ebx, 1	
   mov eax, 4	
   int 80h 

   mov eax, 4
    mov ebx, 1
    mov ecx, msg2
    mov edx, 1
    int 80h

   mov eax, 1 	
   int 80h
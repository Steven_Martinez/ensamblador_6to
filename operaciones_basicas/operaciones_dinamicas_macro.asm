;************Ensamblador*****************
;************6to "A"*********************
;************Angel Steven Martinez Chamba
;************lunes 22 de junio del 2020

%macro imprimir 2 	
	mov eax, 4  		; el sistema interpreta 4 como "escribir" mueve el valor al registro eax
	mov ebx, 1			; salida estándar (imprimir en terminal) 
	mov ecx, %1 		; %1 indica que va a recibir el primer parámetro
	mov edx, %2			; %2 indica que va a recibir el segundo parámetro
	int 80H				; interruccion de kernel linux OS
%endmacro

%macro lectura 1 	
	mov eax, 3          ; define el tipo de operacion (lectura)
    mov ebx, 2          ; estandar de entrada
    mov ecx, %1         ; captura por teclado
    mov edx, 5          ; num de caracteres reservado
    int 80h
%endmacro


section .data  
    msg_num1 db "Ingrese el primer numero", 10
    len_msg1 equ $-msg_num1
    msg_num2 db "Ingrese el segundo numero", 10
    len_msg2 equ $-msg_num2
    msg_num3 db "suma ", 10
    len_msg3 equ $-msg_num3
    msg_num4 db "resta ", 10
    len_msg4 equ $-msg_num4
    msg_num5 db "multiplicacion ", 10
    len_msg5 equ $-msg_num5
    msg_num6 db "division ", 10
    len_msg6 equ $-msg_num6
   

section .bss
    num1 resb 1
    num2 resb 1
    suma resb 1
    resta resb 1
	multi resb 2
	residuo resb 1
	divi resb 1

section .text
    global _start
_start:

    ;***************suma********

    imprimir msg_num1, len_msg1

    lectura num1

    imprimir msg_num2, len_msg2

	lectura num2
    
    ;******************asignacion de la variable resp***************
    mov eax, [num1]
    sub eax, '0'
    mov ebx, [num2]
    sub ebx, '0'
    add eax, ebx
    add eax, '0'
    mov [suma], eax

    ;******************imprime el mensaje***************
    
    imprimir msg_num3, len_msg3

    imprimir suma, 1

    ;***********************resta***********************
	mov ax, [num1]
	mov bx, [num2]
	sub ax, '0'
	sub bx, '0'
	sub ax, bx
	add ax,  '0'
	mov [resta], ax

    ;******************imprime el mensaje***************

    imprimir msg_num4, len_msg4

    imprimir resta, 1

    ;**************************multiplicacion***********
	mov ax, [num1]
	mov bx, [num2]
	sub ax, '0'
	sub bx, '0'
	mul bx
	add ax, '0'
	mov [multi], ax

    ;******************imprime el mensaje***************
    
    imprimir msg_num5, len_msg5

    imprimir multi, 1

    ;*****************division**************************

	mov al, [num1]
	mov bl, [num2]
	sub al, '0'
	sub bl, '0'
	div bl
 
	add al, '0' 	
 	add ah, '0'

	mov [divi], al
	mov [residuo], ah

    ;******************imprime el mensaje***************
    
    imprimir msg_num6, len_msg6
        
    imprimir divi, 1

    ;******************imprime el mensaje***************
    
    imprimir residuo, 1

    ;******************salir del sistema***************
    mov eax, 1                  ; terminacion del sistema
    int 80h                     ; interrucion de OS Linux
    
section .data
	sum db 'La suma de 5+3 es:',10
	len_sum equ $ - sum

	resta db 'La resta de 5-3 es:',10
	len_resta equ $ - resta
	
	mult db 'multiplicacion es:',10
	len_mult equ $ - mult

    divp db 'Division es:',10
	len_divp equ $ - divp
	
section .bss
	res resb 1
    resp resb 1
	resax resb 2
    resdiv resb 1
	residuo resb 1
	cociente resb 1

section .text
	global _start

_start:
	;***************suma********

    ;******************imprime el mensaje***************
	mov eax, 4
	mov ebx, 1
	mov ecx, sum
	mov edx, len_sum
	int 80h
    ;******************lectura de numero***************
	mov eax, 5
	mov ebx, 3
	add eax, ebx
	add eax, '0'
	mov [res], eax
    ;******************imprime el mensaje***************
	mov eax, 4
	mov ebx, 1
	mov ecx, res
	mov edx, 3
	int 80h

	;**********************resta******************
    ;******************imprime el mensaje***************
	mov eax, 4
	mov ebx, 1
	mov ecx, resta
	mov edx, len_resta
	int 80h
    ;******************lectura de numero***************
	mov eax, 5
	mov ebx, 3
	sub eax, ebx
	add eax, '0'
	mov [res], eax
    ;******************imprime el mensaje***************
	mov eax, 4
	mov ebx, 1
	mov ecx, res
	mov edx, 3
	int 80h

    ;multiplicacion
    
    mov al,'3' 	; movimientos a al
	sub al, '0'
	
	mov bl, '2'	; movimientos a bl
	sub bl, '0'

	mul bl		; multiplicación
	add al, '0'
	
	mov [resp], al
	mov [resax], ax
    ;******************imprime el mensaje***************
	mov ecx, mult	
	mov edx, len_mult
	mov eax,4	;lectura del primer mensaje
	mov ebx,1	
	int 0x80	;call kernel
	;******************lectura de numero***************
	mov ecx,resp
	mov edx, 1
	mov eax,4	; lectura del segundo mensaje
	mov ebx,1
	int 80h
    ;******************imprime el mensaje***************
	mov ecx,resax
	mov edx, 1
	mov eax,4	; lectura del segundo mensaje
	mov ebx,1
	int 80h	

   ;******************DIVISION***************
    mov ax,'9'
    sub ax, '0'
	
    mov bl, '2'
    sub bl, '0'

    div bl
    add ax, '0'
	
    mov [resdiv], ax
    ;******************imprime el mensaje***************
    mov ecx, divp	
    mov edx, len_divp
    mov ebx, 1	
    mov eax, 4	
    int 80h	
	;******************lectura de numero***************
    mov ecx, resdiv
    mov edx, 1
    mov ebx, 1	
    mov eax, 4	
    int 80h

    add al, '0'
    mov [residuo], al
    mov ecx, residuo

    mov edx, 1
    mov ebx, 1	
    mov eax, 4	
    int 80h

    add ah, '0'
    mov [cociente], ah
    mov ecx, cociente
    mov edx, 1
    mov ebx, 1	
    mov eax, 4	
    int 80h 

    ;******************salida**************
	mov eax,1
	int 80h
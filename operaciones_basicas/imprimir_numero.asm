section .data
    msg db "Ingrese un numero", 10      ; variable
    msg_len equ $-msg                   ; length msg
    msg_presentacion db "el numero ingresado es", 10
    msg_pr_len equ $-msg_presentacion

section .bss
    num resb 5

section .text
   global _start
_start:    
    ;******************imprime el mensaje***************
    mov eax, 4
    mov ebx, 
    mov ecx, msg
    mov edx, msg_len
    int 80h
    ;******************asingacion a la variable***************
    mov ebx, 7                  ; asigna primero a un registro (ASCII-55)
    add ebx, '0'                ; suma de 0 en codigo ASCII (48)
    mov [num], ebx              ; variable toma el valor contenido en el registro
    ;******************lectura de numero***************
    ;mov eax, 3                  ; define el tipo de operacion (lectura)
    ;mov ebx, 2                  ; estandar de entrada
    ;mov ecx, num                ; captura por teclado
    ;mov edx, 5                  ; num de caracteres reservado
    ;int 80h
    ;******************imprime el mensaje-presentacion***************
    mov eax, 4
    mov ebx, 1
    mov ecx, msg_presentacion
    mov edx, msg_pr_len
    int 80h
    ;******************imprime el numero capturado x teclado***************
    mov eax, 4                  ; (imprimir)
    mov ebx, 1                  ; consola    
    mov ecx, num                ; num
    mov edx, 5                  ; caracteres reservados
    int 80h                     ; interrucion de OS Linux
    ;******************salir del sistema***************
    mov eax, 1                  ; terminacion del sistema
    int 80h                     ; interrucion de OS Linux
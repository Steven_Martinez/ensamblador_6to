%macro imprimir 2 	
	mov eax, 4  		; el sistema interpreta 4 como "escribir" mueve el valor al registro eax
	mov ebx, 1			; salida estándar (imprimir en terminal) 
	mov ecx, %1 		; %1 indica que va a recibir el primer parámetro
	mov edx, %2			; %2 indica que va a recibir el segundo parámetro
	int 80H				; interruccion de kernel linux OS
%endmacro

section .data

    msj db "Ingresa datos en el archivo", 10
    len1 equ $-msj

    archivo db "/home/end/Documentos/ensamblador_6to/archivos/file.txt"

section .bss

    texto resb 30
    fileId resd 1

section .text
    global _start

leer:
    mov eax, 3
    mov ebx, 0
    mov ecx, texto
    mov edx, 10
    int 80h
    ret

_start:

    mov eax, 8          ;lectura o escritura del archivo
    mov ebx, archivo
    mov ecx, 1          ;modo de acceso
                            ; 0-RDONLY 0: El archivo se abre solo para lerr
                            ; 0-WRONLY 1: El archivo se abre para escritura
                            ; 0-RDWR    2: El archivo se abre para escritura y lectura
                            ; 0-CREATE 256: Crea el archivo en caso que no existe
                            ; 0-APPEND 2000h: El archivo se abre para escritura al final
    mov edx, 777h
    int 80h

    test eax, eax
    jz exit             ;se ejecuta cuando existen errores en el archivo

    ;imprimir msj, len1
    mov dword[fileId], eax

    call leer

    mov eax, 4
    mov ebx, [fileId]
    mov ecx, texto
    mov edx, 20
    int 80h

exit:
    mov eax, 1
    int 80h


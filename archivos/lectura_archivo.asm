%macro imprimir 2 	
	mov eax, 4  		; el sistema interpreta 4 como "escribir" mueve el valor al registro eax
	mov ebx, 1			; salida estándar (imprimir en terminal) 
	mov ecx, %1 		; %1 indica que va a recibir el primer parámetro
	mov edx, %2			; %2 indica que va a recibir el segundo parámetro
	int 80h				; interruccion de kernel linux OS
%endmacro

section .data

    msj db "Ingresa datos en el archivo", 10
    len1 equ $-msj

    archivo db "/home/end/Documentos/ensamblador_6to/archivos/file.txt"

section .bss

    texto resb 30
    fileId resd 1

section .text
    global _start
_start:

    mov eax, 5          ;servicio de archivos $-5 operacion de open
    mov ebx, archivo
    mov ecx, 0          
    mov edx, 777h
    int 80h

    test eax, eax
    jz exit             ;se ejecuta cuando existen errores en el archivo

    mov dword[fileId], eax

    imprimir msj, len1

    mov eax, 3
    mov ebx, [fileId]
    mov ecx, texto
    mov edx, 15
    int 80h

    imprimir texto, 15

    mov eax, 6          ;servicio de archivos close
    mov ebx, [fileId]
    mov ecx, 0
    mov edx, 0
    int 80h

exit:
    mov eax, 1
    int 80h
